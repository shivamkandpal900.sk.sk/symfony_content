<?php

namespace App\Repository;

use App\Entity\Presave;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Presave|null find($id, $lockMode = null, $lockVersion = null)
 * @method Presave|null findOneBy(array $criteria, array $orderBy = null)
 * @method Presave[]    findAll()
 * @method Presave[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PresaveRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Presave::class);
    }

    // /**
    //  * @return Presave[] Returns an array of Presave objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Presave
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
