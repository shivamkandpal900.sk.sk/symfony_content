<?php

namespace App\Controller;

use App\Entity\Header;
use App\Form\HeaderType;
use App\Repository\HeaderRepository;

use App\Entity\Footer;
use App\Repository\FooterRepository;

use App\Entity\Content;
use App\Repository\ContentRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dashboard")
 */

class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="dashboard_index", methods={"GET"})
     */
    public function index(FooterRepository $footerRepository , HeaderRepository $headerRepository,ContentRepository $contentRepository): Response
    {
        return $this->render('dashboard/index.html.twig', [
            'headers' => $headerRepository->findAll(),
            'footers' => $footerRepository->findAll(),
            'contents' => $contentRepository->findAll(),
        ]);
        
    }

    /**
     * @Route("/show", name="dashboard_show", methods={"GET"})
     */
    public function show(Request $request, HeaderRepository $headerRepository, FooterRepository $footerRepository, ContentRepository $contentRepository): Response
    {
        //http://127.0.0.1:8000/show?id=1&type=footer
        
        $id = $request->query->get('id');
        $type = $request->query->get('type');

        if($type == 'header'){
            $data = $headerRepository->find($id) ;
        }
       
       elseif($type == 'content'){
        $data = $contentRepository->find($id) ;
       }
       elseif($type == 'footer'){
        $data = $footerRepository->find($id) ;
       }
        return $this->render('dashboard/show.html.twig', [
            'data' => $data
      ]);
     
    }
    }

